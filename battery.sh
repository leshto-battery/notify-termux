# If there's an already settled BWD variable, let's check it!

FRATE=1
URATE="A"
FTEMP=1

battery_parse_status() {
  case "$status" in
    "FULL") status="Full" ;;
    "CHARGING") status="Charging" ;;
    "DISCHARGING") status="Discharging" ;;
    "NOT CHARGING") status="Not charging" ;;
    *) status="Unknown" ;;
  esac
}

battery_get_status() {
  local text="$(termux-battery-status)"
  percent=$(echo $text | sed -E 's/.*\"percentage\": ([0-9]+),.*/\1/g')

  status=$(echo $text | sed -E 's/.*"status": "([[:alpha:] ]+)",.*/\1/g')
  battery_parse_status

  temp_int=$(echo $text | sed -E 's/.*"temperature": ([[:digit:]]+).+/\1/g')
  temp_dec=$(echo $text | sed -E 's/.*"temperature": .+\.([[:digit:]]).*/\1/g')

  current=$(echo $text | sed -E 's/.*"current": (-?[[:digit:]]+).*/\1/g')
}

