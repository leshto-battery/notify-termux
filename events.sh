#    Copyright © 2021, 2022 Cledson Ferreira
#
#    This file is part of Notify para Termux.
#
#    Notify para Termux is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Notify para Termux is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Notify para Termux.  If not, see <https://www.gnu.org/licenses/>

. "$LIB/battery.sh"


pre_fix_percent() {
  local zero="$S_BTWEAK_EMPTY"
  local cent="$S_BTWEAK_FULL"
  [ -z "$zero" -o -z "$cent" ] && return

  local pf=$(expr '(' $percent - $zero ')' '*' 100 / '(' $cent - $zero ')')
  [ "$pf" -le 0 ] && percent=0 || percent=$pf
  [ "$pf" -ge 100 ] && percent=100 || percent=$pf
}

# Print log message in Notify format:
# <percent value> % (<power_supply status>)[ | <current> <mA|mW>][ | <voltage> mV][ | <temperature> ºC]
log_status() {
  printf "$percent %% ($status)"
  [ -n "$FRATE" ] && printf " | $current m$URATE"
  [ -n "$FVOLTAGE" ] && printf " | $voltage mV"
  [ -n "$FTEMP" ] && printf " | $temp_int.$temp_dec °C"
  echo ""
}

events_main() {
  battery_get_status
  pre_fix_percent
  log_status

  [ "$status" = "Charging" ] && return $TRUE
}
