#!/bin/sh

mkdir -p "$PREFIX/lib/batservice"
cp *.sh "$PREFIX/lib/batservice/"
chmod +x "$PREFIX/lib/batservice/notify.sh"
chmod +x "$PREFIX/lib/batservice/service.sh"

ln -s "$PREFIX/lib/batservice/notify.sh" "$PREFIX/bin/batservice"

mkdir -p "$PREFIX/etc/batservice/"
touch "$PREFIX/etc/batservice/config"

echo "done"
