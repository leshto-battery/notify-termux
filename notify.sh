#!/bin/bash

#    Notify para Termux - indicador de bateria via Termux:API
#
#    Copyright © 2021, 2022 Cledson Ferreira
#
#    Notify para Termux is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Notify para Termux is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Notify para Termux.  If not, see <https://www.gnu.org/licenses/>.

if [ $(id -u) -eq 0 ]; then
  echo "ERRO: Notify não pode ser executado em modo root!"
  exit 3
fi

sh=sh

# Find the common library
if [ ! -r "$LIB_FIX/common.sh" ]; then
  [ -z "$PREFIX" ] && exit 3
  LIB="$PREFIX/lib/batservice"
else
  LIB="$LIB_FIX"
fi

. "$LIB/common.sh"

# Create needed directories and bs.pid to service work
mkdir -p "$CACHE" "$DATA"
touch "$FPID"

# PRELUDE == Common knowledge shared between sections 1 and 2

termux-notification -h >/dev/null 2>&1
[ $? -eq 0 ] && HAS_API=1

# Launch termux-api commands without blocking process
# Check BatService's source for details
spawn_and_kill() {
  local tl
  "$@" &
  tl=7
  while [ $tl -gt 0 ]; do
    tl=$(expr $tl - 1)
    jobs %1 >/dev/null 2>&1
    [ $? -ne 0 ] && break
    # TODO: try this: [ $tl -gt 0 ] || pkill termux-api
    sleep 1
  done
}

send_toast() {
  if [ -z "$HAS_API" ]; then
    echo "TOAST: $1"
    return 0
  fi
  spawn_and_kill termux-toast "$1"
}


# SECTION 1 == Notify service controller

run_service() {
#  if [ "$(uname -o)" = "Android" ]; then
#    su -c sh "$LIB/service.sh" | bash "$LIB/notify.sh" 2>> "$CACHE/bs.err"
#  else
    $sh "$LIB/service.sh" | $sh "$LIB/notify.sh" >/dev/null
#  fi
  return $?
}

notify_controller() {
  case "$1" in
    squit)
      echo '' > "$FPID"
      ;;
    quit)
      pid=$(cat "$FPID")
      [ -z "$pid" ] || kill $pid
      [ $? -ne 0 ] && echo '' > "$FPID"
      send_toast "Serviço encerrado"
      ;;
    restart|start)
      [ "$1" = "restart" ] && send_toast "Reiniciando..."
      run_service
      ;;
    *)
      echo "USO: $0 [squit | quit | restart]"
      ;;
  esac
  exit 0
}

[ -n "$1" ] && notify_controller "$@"

case "$0" in *batservice)
  run_service
  exit $?
  ;;
esac


# SECTION 2 == Battery's persistent indicator service: Notify para Termux.

send_status() {
  if [ -z "$HAS_API" ]; then
    echo "STATUS: $1"
    return 0
  fi

  local icon="battery_std"
  if [ "comment" != "all" ]; then
    percent="$(echo $percent | grep -Eo '[[:digit:]]+')"
    case "$status" in
      '(Full)'|'(Charging)'|'(Not charging)') icon="charger" ;;
      '(Discharging)')
        icon="battery_alert"
        if [ "$percent" -ge 25 ]; then icon="battery_std"; fi
        ;;
      *) icon="battery_unknown" ;;
    esac
  fi


  WORKAROUND="LIB_FIX=$LIB DATA_FIX=$DATA"
  spawn_and_kill termux-notification -i batservice --ongoing --icon "$icon" \
    --alert-once -t "Estado da bateria" -c "$1" \
    --button1 "reiniciar" --button1-action "$WORKAROUND sh $LIB/notify.sh restart" \
    --button2 "encerrar" --button2-action "$WORKAROUND sh $LIB/notify.sh quit"
}

status_quit() {
  [ -z "$HAS_API" ] && return 0
  termux-notification-remove batservice
}

update_status() {
  local status_txt
  case "$status" in
    "(Discharging)"|"") status_txt="🔋 " ;;
    "(Not charging)"|"(Charging)") status_txt="🔌 " ;;
    *) status_txt="⚠️ " ;;
  esac
  status_txt="$status_txt$percent"
  [ -n "$current" ] && status_txt="$status_txt ($current)"
  [ -n "$voltage" ] && status_txt="$status_txt ⚡ $voltage"
  [ -n "$temp" ] && status_txt="$status_txt 🌡 $temp"
  send_status "$status_txt"
}

parse_status() {
  percent="$(echo $stdin | grep -Eo '[[:digit:]]+ %')"
  [ -z "$percent" ] && return $FALSE
  status="$(echo $stdin | grep -Eo '\([[:alnum:] ]+\)')"
  current="$(echo $stdin | grep -Eo '\-{0,1}[[:digit:]]+ (mA|mW)')"
  voltage="$(echo $stdin | grep -Eo '\-{0,1}[[:digit:]]+ mV')"
  temp="$(echo $stdin | grep -Eo '\-{0,1}[[:digit:]]+[[:punct:]][[:digit:]] .C')"
  return $TRUE
}

parse_message() {
  msg="$(echo $stdin | grep -Eo '#')"
  [ -z "$msg" ] && return $FALSE
  type="$(echo $stdin | sed -E 's/#(toast|warn|error|debug) .+/\1/g')"
  msg="$(echo $stdin | sed -E 's/#(toast|warn|error|debug) (.+)/\2/g')"
}

update_message() {
  if [ -z "$HAS_API" ]; then
    echo "MSG-$type: $msg"
    return 0
  fi

  case "$type" in
    toast)
      send_toast "BatService: $msg"
      ;;

    warn)
      spawn_and_kill termux-notification --icon battery_std --group warn\
        -t "Alerta do serviço do Notify" -c "$msg"
      send_toast "BatService: $msg"
      ;;

    error)
      spawn_and_kill termux-notification --icon battery_std --group error\
/        -t "Erro no serviço do Notify" -c "$msg"
      send_toast "BatService [!!!]: $msg"
      echo "MSG-error: $msg" >&2
      ;;
    *)
      spawn_and_kill termux-notification --icon battery_std --group debug\
       -t "Mensagem do serviço do Notify" -c "$msg"
      ;;
  esac
}

# Begin the service to read and process each line from standard input

while [ 0 -eq 0 ]; do
  read stdin || break

  if parse_message; then update_message
  elif parse_status; then update_status # && echo "$stdin"
  else
    type="error"
    msg="$stdin"
    update_message
  fi
done

status_quit
