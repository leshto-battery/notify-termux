# Default/recommended settings
config_set_default() {
  S_DELAY_NOT_CHARGING=60
  S_DELAY=6
  S_BTWEAK_EMPTY=0
  S_BTWEAK_FULL=100
  S_ALERT_LOW=15
  S_ALERT_HIGH=80
  S_ALERT_COLD=5
  S_ALERT_HOT=40
}

config_set_default

CONFIG_FILE="$DATA/config"
# Use `unset S_option` to disable an option

cflast=0
config_refresh() {
	if [ -r "$CONFIG_FILE" ]; then
		local ftime="$(stat -c %Y "$CONFIG_FILE")"
		if [ "$ftime" -gt "$cflast" ]; then
			. "$CONFIG_FILE"
			config_check
			cflast=$ftime
			echo "#toast configuração carregada"
		fi
	fi
}

config_check() {
	x="$S_DELAY_NOT_CHARGING"
	[ -n "$x" ] && [ "$x" -gt 600 -o "$x" -lt 6 ] && S_DELAY_NOT_CHARGING=60
	x="$S_DELAY"
	[ -n "$x" ] && [ "$x" -gt 60 -o "$x" -lt 1 ] && S_DELAY=6

	x="$S_BTWEAK_EMPTY"
	y="$S_BTWEAK_FULL"
	if [ -z "$x" -o -z "$y" -o "$x" -lt 0 -o "$x" -ge "$y" -o "$y" -gt 100 ]; then
		unset S_BTWEAK_EMPTY
		unset S_BTWEAK_FULL
	fi

	x="$S_ALERT_LOW"
	y="$S_ALERT_HIGH"
	if [ -z "$x" -o -z "$y" -o "$x" -le 0 -o "$x" -ge "$y" -o "$y" -ge 100 ]; then
		unset S_ALERT_LOW
		unset S_ALERT_HIGH
	fi

	x="$S_ALERT_COLD"
	y="$S_ALERT_HOT"
	if [ -z "$x" -o -z "$y" ]; then
		unset S_ALERT_COLD
		unset S_ALERT_HOT

	elif [ "$x" -le 5 -o "$x" -ge "$y" ] && [ "$y" -gt 59 ]; then
		S_ALERT_COLD=5
		S_ALERT_HOT=40
	fi
}
