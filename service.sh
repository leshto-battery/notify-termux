#!/bin/sh

#    Copyright © 2021, 2022 Cledson Ferreira
#
#    This file is part of Notify para Termux.
#
#    Notify para Termux is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Notify para Termux is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Notify para Termux.  If not, see <https://www.gnu.org/licenses/>

# Find the common library
if [ -z "$LIB_FIX" -o ! -r "$LIB_FIX/common.sh" ]; then
  [ -z "$PREFIX" ] && exit 3
  LIB="$PREFIX/lib/batservice"
else
  LIB="$LIB_FIX"
fi

. "$LIB/common.sh"

# Notify should create needed directories and bs.pid file to avoid SELinux of blocking access by "user application" (i.e., Termux) to the file
if [ ! -f "$FPID" ]; then
  # Sleep for a second before checking if that file has been created
  sleep 1

  if [ ! -f "$FPID" ]; then
    echo "#error Arquivo $FPID não encontrado!"
    exit 1
  fi
fi

# Check if there's a PID in the file and terminate the referred process
pid=$(cat "$FPID")
if [ -n "$pid" ] && [ -r "/proc/$pid/comm" ]; then
  kill $pid

  for i in 1 2 3; do
    pid=$(cat "$FPID")
    [ -z "$pid" ] && break
    sleep 1
  done

  if [ -n "$pid" ]; then
    echo "#error Erro ao tentar encerrar serviço. Tente novamente"
    echo "" > "$FPID"
    exit 9
  fi
fi

# Writes PID of itself in the file when ready
echo $$ > "$FPID"

on_term() {
  echo "" > "$FPID"
  [ -n "$spid" ] && kill "$spid"
  exit 0
}

# We gonna receive signals from external process in order to deliver responses as soon as possible
trap on_term 1 2 15 2>/dev/null


# Load remaining application libraries to begin the service
. "$LIB/config.sh"
. "$LIB/events.sh"

while [ 0 -eq 0 ]; do
  pid=$(cat "$FPID")
  [ "$pid" -ne $$ ] && break

  config_refresh
  events_main
  if [ $? -eq $TRUE ]; then
    sleep $S_DELAY &
    spid=$!
  else
    sleep $S_DELAY_NOT_CHARGING &
    spid=$!
  fi
  wait $spid
  unset spid
done
