#    Copyright © 2021, 2022 Cledson Ferreira
#
#    This file is part of Notify para Termux.
#
#    Notify para Termux is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Notify para Termux is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Notify para Termux.  If not, see <https://www.gnu.org/licenses/>

# When entering in root mode on Android, HOME folder is altered to the root of the file system.
# For least we keep with the PREFIX env var.
[ -n "$HOME_FIX" ] && HOME="$HOME_FIX"
if [ "$HOME" = "/" ]; then
  HOME="$(cd $PREFIX/../home && pwd)"
fi

CACHE="$HOME/.cache/BatService"
FPID="$CACHE/bs.pid"

if [ -n "$DATA_FIX" ]; then
  DATA="$DATA_FIX"
else
  DATA="$PREFIX/etc/batservice"
fi

test 0 -eq 0
TRUE=$?
test 0 -ne 0
FALSE=$?
